'use strict';

var assert = require('chai').assert;
var HTTPProvider = require('ethjs-provider-http'); // eslint-disable-line
var SignerProvider = require('../index.js'); // eslint-disable-line
var Eth = require('ethjs-query'); // eslint-disable-line
var Web3 = require('web3'); // eslint-disable-line
var HttpProvider = require('ethjs-provider-http');
var TestRPC = require('ethereumjs-testrpc');
var sign = require('ethjs-signer').sign;
var server = TestRPC.server({
  accounts: [{
    secretKey: '0xc55c58355a32c095c7074837467382924180748768422589f5f75a384e6f3b33',
    balance: '0x0000000000000056bc75e2d63100000'
  }]
});
server.listen(5012);

describe('SignerProvider', function () {
  describe('constructor', function () {
    it('should construct properly', function (done) {
      var provider = new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          var sigpack = sign(rawTx, '0xc55c58355a32c095c7074837467382924180748768422589f5f75a384e6f3b33');

          cb(null, sigpack);
        }
      });

      assert.equal(typeof provider, 'object');
      assert.equal(typeof sign, 'function');
      assert.equal(typeof provider.options, 'object');
      assert.equal(typeof provider.options.signTransaction, 'function');
      assert.equal(provider.timeout, 0);

      setTimeout(function () {
        done();
      }, 3000);
    });

    it('should throw errors when improperly constructed', function () {
      assert.throws(function () {
        return SignerProvider('http://localhost:5012', {});
      }, Error); // eslint-disable-line
      assert.throws(function () {
        return new SignerProvider('http://localhost:5012', 22);
      }, Error);
      assert.throws(function () {
        return new SignerProvider('http://localhost:5012', {});
      }, Error);
      assert.throws(function () {
        return new SignerProvider('http://localhost:5012');
      }, Error);
    });
  });

  describe('functionality', function () {
    it('should perform normally for calls', function (done) {
      var provider = new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          return cb(null, sign(rawTx, '0xc55c58355a32c095c7074837467382924180748768422589f5f75a384e6f3b33'));
        }
      });
      var eth = new Eth(provider);

      eth.accounts(function (accountsError, accounts) {
        assert.equal(accountsError, null);
        assert.equal(typeof accounts, 'object');
        assert.equal(Array.isArray(accounts), true);

        eth.getBalance(accounts[0], function (balanceError, balanceResult) {
          assert.equal(balanceError, null);
          assert.equal(typeof balanceResult, 'object');

          done();
        });
      });
    });

    it('should perform normally for calls with tx count and gas price', function (done) {
      var eth = new Eth(new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          return cb(null, sign(rawTx, '0xc55c58355a32c095c7074837467382924180748768422589f5f75a384e6f3b33'));
        }
      }));

      eth.accounts(function (accountsError, accounts) {
        assert.equal(accountsError, null);
        assert.equal(typeof accounts, 'object');
        assert.equal(Array.isArray(accounts), true);

        eth.getBalance(accounts[0], function (balanceError, balanceResult) {
          assert.equal(balanceError, null);
          assert.equal(typeof balanceResult, 'object');

          done();
        });
      });
    });

    it('should reconstruct sendTransaction as sendRawTransaction', function (done) {
      var provider = new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          return cb(null, sign(rawTx, '0xc55c58355a32c095c7074837467382924180748768422589f5f75a384e6f3b33'));
        }
      });
      var eth = new Eth(provider);

      eth.accounts(function (accountsError, accounts) {
        assert.equal(accountsError, null);
        assert.equal(Array.isArray(accounts), true);

        eth.sendTransaction({
          from: accounts[0],
          to: '0xc55c58355a32c095c70748374673829241807487',
          data: '0x',
          value: 5000,
          gas: 300000
        }, function (txError, txHash) {
          assert.equal(txError, null);
          assert.equal(typeof txHash, 'string');

          setTimeout(function () {
            eth.getBalance('0xc55c58355a32c095c70748374673829241807487').then(function (balanceResult) {
              assert.equal(typeof balanceResult, 'object');
              assert.equal(balanceResult.toNumber(10), 5000);

              done();
            });
          }, 500);
        });
      });
    });

    it('should handle invalid nonce', function (done) {
      var baseProvider = new HttpProvider('http://localhost:5012');
      var provider = new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          return cb(new Error('account does not have permission'));
        },
        provider: function Provider() {
          var self = this;
          self.sendAsync = function (payload, cb) {
            if (payload.method === 'eth_getTransactionCount') {
              cb(new Error('invalid nonce'), null);
            } else {
              baseProvider.sendAsync(payload, cb);
            }
          };
        }
      });
      var eth = new Eth(provider);

      eth.accounts(function (accountsError, accounts) {
        assert.equal(accountsError, null);
        assert.equal(Array.isArray(accounts), true);
        done();
      });
    });

    it('should handle valid accounts option', function (done1) {
      var provider = new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          return cb(null, sign(rawTx, '0xc55c58355a32c095c7074837467382924180748768422589f5f75a384e6f3b33'));
        },
        accounts: function accounts(cb) {
          return cb(null, ['0xc55c58355a32c095c70748374673829241807487']);
        }
      });
      var eth = new Eth(provider);

      eth.accounts(function (accountsError, accounts1) {
        assert.equal(accountsError, null);
        assert.equal(Array.isArray(accounts1), true);

        done1();
      });
    });

    it('should handle invalid gas price', function (done) {
      var baseProvider = new HttpProvider('http://localhost:5012');
      var provider = new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          return cb(null, sign(rawTx, '0xc55c58355a32c095c7074837467382924180748768422589f5f75a384e6f3b33'));
        },
        provider: function Provider() {
          var self = this;
          self.sendAsync = function (payload, cb) {
            if (payload.method === 'eth_gasPrice') {
              cb(new Error('invalid nonce'), null);
            } else {
              baseProvider.sendAsync(payload, cb);
            }
          };
        }
      });
      var eth = new Eth(provider);

      eth.accounts(function (accountsError, accounts) {
        assert.equal(accountsError, null);
        assert.equal(Array.isArray(accounts), true);

        eth.sendTransaction({
          from: accounts[0],
          to: '0xc55c58355a32c095c70748374673829241807487',
          data: '0x',
          gas: 300000
        })['catch'](function (txError) {
          assert.equal(typeof txError, 'object');

          done();
        });
      });
    });

    it('should handle invalid tx count', function (done) {
      var baseProvider = new HttpProvider('http://localhost:5012');
      var provider = new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          return cb(null, sign(rawTx, '0xc55c58355a32c095c7074837467382924180748768422589f5f75a384e6f3b33'));
        },
        provider: function Provider() {
          var self = this;
          self.sendAsync = function (payload, cb) {
            if (payload.method === 'eth_getTransactionCount') {
              cb(new Error('invalid nonce'), null);
            } else {
              baseProvider.sendAsync(payload, cb);
            }
          };
        }
      });
      var eth = new Eth(provider);

      eth.accounts(function (accountsError, accounts) {
        assert.equal(accountsError, null);
        assert.equal(Array.isArray(accounts), true);

        eth.sendTransaction({
          from: accounts[0],
          to: '0xc55c58355a32c095c70748374673829241807487',
          data: '0x',
          gas: 300000
        })['catch'](function (txError) {
          assert.equal(typeof txError, 'object');

          done();
        });
      });
    });

    it('should throw an error when key error is provided', function (done) {
      var provider = new SignerProvider('http://localhost:5012', {
        signTransaction: function signTransaction(rawTx, cb) {
          return cb(new Error('account does not have permission'));
        }
      });
      var eth = new Eth(provider);

      eth.accounts(function (accountsError, accounts) {
        assert.equal(accountsError, null);
        assert.equal(Array.isArray(accounts), true);

        eth.sendTransaction({
          from: accounts[0],
          to: '0xc55c58355a32c095c70748374673829241807487',
          data: '0x',
          gas: 300000
        })['catch'](function (txError) {
          assert.equal(typeof txError, 'object');

          done();
        });
      });
    });
  });
});