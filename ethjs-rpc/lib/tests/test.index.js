'use strict';

var EthRPC = require('../index.js');
var assert = require('chai').assert;
var TestRPC = require('ethereumjs-testrpc');
var provider = TestRPC.provider({});
var provider2 = TestRPC.provider({});

describe('ethjs-rpc', function () {
  describe('construction', function () {
    it('should construct normally', function () {
      var eth = new EthRPC(provider);

      assert.equal(typeof eth, 'object');
      assert.equal(typeof eth.currentProvider, 'object');
      assert.equal(typeof eth.options, 'object');
    });

    it('should throw when invalid construction params', function () {
      assert.throws(function () {
        return EthRPC(provider);
      }, Error); // eslint-disable-line
    });
  });

  describe('setProvider', function () {
    it('should change provider', function (done) {
      var eth = new EthRPC(provider);
      eth.sendAsync({ method: 'eth_accounts' }, function (err, accounts1) {
        assert.equal(err, null);
        eth.setProvider(provider2);

        eth.sendAsync({ method: 'eth_accounts' }, function (err2, accounts2) {
          assert.equal(err2, null);
          assert.notDeepEqual(accounts1, accounts2);
          done();
        });
      });
    });

    it('should handle invalid provider', function () {
      assert.throws(function () {
        return new EthRPC(23423);
      }, Error);
    });
  });

  describe('sendAsync', function () {
    it('should handle normal calls', function (done) {
      var eth = new EthRPC(provider);
      eth.sendAsync({ method: 'eth_accounts' }, function (err, accounts1) {
        assert.equal(err, null);
        assert.equal(Array.isArray(accounts1), true);
        assert.equal(accounts1.length > 0, true);
        done();
      });
    });

    it('should handle invalid response', function (done) {
      var eth = new EthRPC({ sendAsync: function sendAsync(payload, cb) {
          cb(null, { error: 'Some Error!!' });
        } });
      eth.sendAsync({ method: 'eth_accounts' }, function (err, accounts1) {
        assert.equal(typeof err, 'object');
        assert.equal(accounts1, null);
        done();
      });
    });

    it('should handle invalid errors', function (done) {
      var eth = new EthRPC({ sendAsync: function sendAsync(payload, cb) {
          cb('Some error!');
        } });
      eth.sendAsync({ method: 'eth_accounts' }, function (err, accounts1) {
        assert.equal(typeof err, 'object');
        assert.equal(accounts1, null);
        done();
      });
    });
  });
});