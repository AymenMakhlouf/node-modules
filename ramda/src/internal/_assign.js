var _objectAssign = /*#__PURE__*/require('./_objectAssign');

module.exports = typeof Object.assign === 'function' ? Object.assign : _objectAssign;